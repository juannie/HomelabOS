---
- name: Make mailserver directory.
  file:
    path: "{{ volumes_root }}/mailserver"
    state: directory

- name: Copy mailserver docker-compose.yml file into place.
  template:
    src: docker-compose.mailserver.yml.j2
    dest: "{{ volumes_root }}/mailserver/docker-compose.mailserver.yml"
  vars:
    tor_domain: "{{ tor_http_domain_file.stdout | default('') }}"

- name: Make mailserver::postfix directory.
  file:
    path: "{{ volumes_root }}/mailserver/mail/postfix"
    state: directory

- name: Copy root postfix settings into place
  copy:
    src: config.inc.php
    dest: "{{ volumes_root }}/mailserver/mail/postfix/config.inc.php"

- name: Copy user postfix settings into place
  copy:
    src: config.local.php
    dest: "{{ volumes_root }}/mailserver/mail/postfix/config.local.php"

- name: Configure mailserver systemd service.
  template:
    src: service.j2
    dest: /etc/systemd/system/mailserver.service

- name: Configure postfix to forward to an external SMTP server
  lineinfile:
    path: "{{ volumes_root }}/mailserver/mail/postfix/custom.conf"
    line: "{{ item }}"
    regexp: "{{ item.split('=')[0] }}"
  with_items:
    - "smtp_sasl_auth_enable = yes"
    - "smtp_sasl_password_maps = hash:/var/mail/postfix/sasl_password"
    - "smtp_sasl_security_options = noanonymous"
    - "smtp_sasl_tls_security_options = noanonymous"
    - "smtp_tls_security_level = encrypt"
    - "header_size_limit = 4096000"
    - "relayhost = [{{ smtp.host }}]:{{ smtp.port }}"
  become: true
  when: mailserver.forward_to_smtp

- name: configure sasl
  template:
    src: sasl_password.j2
    dest: "{{ volumes_root }}/mailserver/mail/postfix/sasl_password"
    owner: root
    group: root
    mode: 0600
  become: true
  register: mail_sasl_result
  when: mailserver.forward_to_smtp

- name: Start mailserver
  systemd:
    name: mailserver
    enabled: "yes"
    daemon-reload: "yes"
    state: started

- name: Update postfix password hashes
  shell: docker exec mailserver_mailserver_1 /usr/sbin/postmap /var/mail/postfix/sasl_password
  when: mailserver.forward_to_smtp

- name: Reload postfix
  shell: docker exec mailserver_mailserver_1  postfix reload
  when: mailserver.forward_to_smtp
